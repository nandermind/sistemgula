	@extends('layouts.template_superuser')
	@section('header')
		<p>USER LIST - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/user/insert','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
			<tr>
				<td><p> Personal Details </p></td>
			</tr>
			<tr>
					<td>{!!Form::label('name','User ID') !!}</td>
					<td>
						<input type="text" required name="name"></input>
					</td>
			</tr>
			<tr>
				<td><p> Account Details </p></td>
			</tr>
			<tr>
					<td>{!!Form::label('password','Password') !!}</td>
					<td>
						{!!form::password('password')!!}
					</td>
			</tr>
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection
