	@extends('layouts.template_superuser')
	@section('header')
	<br>
		<p>AccessRight - Search Result</p>
	@endsection
	@section('content')
		<br><br>
		<div>
			<a class="btn btn-success" href="{{ url('/access_right')}}"><span class="glyphicon glyphicon-left">Return to Index</span></a>
		</div>
		<br><br>
		<div>
				<table class="table table-responsive" style="width:100%">
					<tr>
						<td>Code</td>
						<td>Description</td>
						<td>Action</td>
					</tr>
		</div>
		<div>
				@foreach ($list as $data)
					<tr>
							<td>{{ $data->code }}</td>
							<td>{{ $data->description }}</td>
							<td>
								<a href="{{ url('/access_right/updating',$data->id)}}"class="btn btn-success"><span class="glyphicon glyphicon-pencil"> EDIT</span></a>
								<a href="{{ url('/access_right/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-remove"> Delete </span></a>
							</td>
					</tr>
				@endforeach
			</table>
			{!! $list->render()!!}
		</div>	
	@endsection
