<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\User;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Hash;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class UserController extends controller
	{
		public function showUser()
			{
				$list = User::paginate(15);
				return view('users.index')->with('list',$list);
			}
		public function showNewUser()
			{
				$obj = new User();
				return view('users.create_form')->with('obj',$obj);
			}
		public function createNewUser(Request $request)
			{
					$obj = new User($request->all());
					$obj->accessRight = "User";
					$obj->password = $request->password;
					$obj->iduser = $request->name;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/user');
	        }
		public function deleteUser($id)
			{
				$User  		   	  = User::find($id);
				$User->delete();
				$logs			  = Log::where("iduser","=",$id);
				$logs->delete();
				Session::flash('message', 'Successfully Updated User Status');
				return redirect('/user');
			}
	}

?>