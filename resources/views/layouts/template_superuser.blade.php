<!DOCTYPE html>
<html lang="en">
<head>
  <title>GiziTest App</title>
  @include('layouts.css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">
      <!-- Logo -->
      <a href="{{url('index')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>S</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Lib</b>Sys</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
              <!-- Menu toggle button -->
            </ul>
          </li><!-- /.messages-menu -->
          <!-- Notifications Menu -->
        </li>
      </ul>
    </div>
  </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form (Optional) -->
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
      <li class="active"><a href="{{url('index')}}"><span class="glyphicon glyphicon-home"> Home</span></a></li>
      <li class="treeview">
        <a href="#"></i> <span class="glyphicon glyphicon-wrench"> Components </span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
          <li><a href="/foods">Foods</a></li>
        </ul>
      </li>
        <li class="treeview">
          <a href="#"><span class="glyphicon glyphicon-user"> User Management</span> <i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="/user">User</a></li>
          </ul>
        </li>
        <li><a href="#"><span class="glyphicon glyphicon-plus"> Add Daily Glucose Intake </span></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-file"> Report </span></a></li>
        <li><a href="/logout"><span class="glyphicon glyphicon-log-out"> Logout </span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('header')
    @if (Session::has('flash_notification.message'))
    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

      {{ Session::get('flash_notification.message') }}
    </div>
    @endif
    <section class="content-header">
      <h1>
        @yield('content')
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      @yield('content2')
    </section><!-- /.content -->
  </div>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
    </div>
    @yield('footer')
    <strong>Copyright &copy; 2-2-</strong> All rights reserved<br>
  </footer>
</body>
</html>
