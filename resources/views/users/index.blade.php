	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>GiziTest - USER LIST</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/user/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<th>User ID</th>
					<th>Access Right</th>
					<th>Actions</th>
				</tr>
			</thead>
				</div>
				<div>
					@foreach ($list as $data)
					<tr>
						<td>{{ $data->iduser }}</td>
						<td>{{ $data->accessRight }}</td>
							</td>
							<td>
								@if ($data->iduser != "administrator")
								<a href="{{ url('/user/drop',$data->id)}}"class="btn btn-warning"><span class="glyphicon glyphicon-pencil"> Activate </span></a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					{!! $list->render()!!}
				</div>	
				@endsection
