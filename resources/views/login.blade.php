    @extends('layouts.login')
    @section('header')
        <p>LOGIN</p>
    @endsection
    @section('content')
        {!!Form::open(array('url' => '/login','method' => 'POST'))!!}
                <tr>
                    <td>{!!Form::label('email', 'email')!!}</td>
                    <td>{!!Form::text('email')!!}</td>
                </tr>
                <tr>
                    <td>{!!Form::label('password', 'password')!!}</td>
                    <td>{!!Form::password('password')!!}</td>
                </tr>
                <tr>
                    <td colspan="2">{!!Form::submit('login')!!}</td>
                </tr>
        {!!Form::close()!!}
    @endsection