<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->string("iduser")->unique();
            $table->string("password");
            $table->string("accessRight");
        });
        DB::table('user')->insert(
            array(
                'iduser' => 'administrator',
                'password' => 'administrator',
                'accessRight' => 'sysadmin',
            )
        );
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
