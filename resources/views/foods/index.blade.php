	@extends('layouts.template_superuser')
	@section('header')
	<br>
	<p>GiziTest - USER LIST</p>
	@endsection
	@section('content')
	<br><br>
	<div>
		<a class="btn btn-success" href="{{ url('/foods/new')}}"><span class="glyphicon glyphicon-plus">Create</span></a>
	</div>
	<br><br>
	<div>
		<table id="dtb" class="table table-responsive" style="width:100%">
			<thead>
				<tr>
					<th>Food Name</th>
					<th>Glucose Count</th>
					<th>Actions</th>
				</tr>
			</thead>
				</div>
				<div>
					@foreach ($list as $data)
					<tr>
						<td>{{ $data->foodname }}</td>
						<td>{{ $data->glucoseCount }}</td>
							</td>
							<td>
								<a href="{{ url('/foods/drop',$data->idfoods)}}"class="btn btn-warning"><span class="glyphicon glyphicon-pencil"> Delete </span></a>
							</td>
						</tr>
						@endforeach
					</table>
					{!! $list->render()!!}
				</div>	
				@endsection
