-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2016 at 10:50 AM
-- Server version: 5.6.26
-- PHP Version: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libsapp`
--
CREATE DATABASE IF NOT EXISTS `libsapp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `libsapp`;

-- --------------------------------------------------------

--
-- Table structure for table `access_rights`
--

DROP TABLE IF EXISTS `access_rights`;
CREATE TABLE IF NOT EXISTS `access_rights` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `access_rights`
--

INSERT INTO `access_rights` (`id`, `code`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Super User', 'Superuser', 1, '2016-02-15 14:58:17', '2016-02-15 14:58:17'),
(2, 'Admin', 'Admin', 1, '2016-02-15 14:58:17', '2016-02-15 14:58:17'),
(3, 'Librarian', 'Librarian', 1, '2016-02-15 14:58:17', '2016-02-15 14:58:17'),
(4, 'Sales', 'Sales', 1, '2016-02-15 14:58:17', '2016-02-15 14:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(10) unsigned NOT NULL,
  `book_tittle` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `book_desc` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `categories` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isbn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `issue_date` datetime NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(18,2) NOT NULL,
  `for_sale` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `book_tittle`, `book_desc`, `categories`, `is_active`, `created_at`, `updated_at`, `isbn`, `issue_date`, `qty`, `price`, `for_sale`) VALUES
(1, 'Apa itu hidup', '', 1, 1, '2016-02-15 08:35:13', '2016-02-15 15:35:13', '', '0000-00-00 00:00:00', 0, 0.00, 0),
(2, 'JUDUL', 'DESKRIPSI', 1, 1, '2016-02-15 20:48:10', '2016-02-16 03:48:10', 'ISBN', '2016-02-16 00:00:00', 1, 2000.00, 0),
(3, 'JUDUL2', 'DESKRIPSI2', 1, 0, '2016-02-15 20:48:41', '2016-02-15 21:00:21', 'ISBN2', '2016-02-16 00:00:00', 1, 2000.00, 0),
(4, 'JUDUL23', 'DESKRIPSI23', 1, 0, '2016-02-15 20:49:38', '2016-02-15 21:01:46', 'ISBN23', '2016-02-29 00:00:00', 2, 42000.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `code`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Keabadian', 'Keabadian', 1, '2016-02-15 08:34:57', '2016-02-15 15:34:57'),
(2, 'Out of Sto', 'Out of Stock', 1, '2016-02-16 01:57:07', '2016-02-16 08:57:07');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

DROP TABLE IF EXISTS `details`;
CREATE TABLE IF NOT EXISTS `details` (
  `id` int(10) unsigned NOT NULL,
  `lending_id` int(10) unsigned NOT NULL,
  `book` int(10) unsigned NOT NULL,
  `time_start` datetime NOT NULL,
  `due_time` datetime NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `total` double(18,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`id`, `lending_id`, `book`, `time_start`, `due_time`, `status`, `total`, `is_active`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '2016-02-01 00:00:00', '2016-02-16 00:00:00', 2, 0.00, 0, '2016-02-15 08:43:53', '2016-02-15 08:50:38'),
(3, 1, 1, '2016-02-01 00:00:00', '2016-02-17 00:00:00', 2, 0.00, 0, '2016-02-15 08:44:03', '2016-02-15 08:56:45'),
(4, 1, 1, '2016-02-01 00:00:00', '2016-02-16 00:00:00', 2, 0.00, 0, '2016-02-15 08:44:44', '2016-02-15 09:02:41'),
(5, 1, 1, '2016-02-02 00:00:00', '2016-02-15 00:00:00', 2, 0.00, 0, '2016-02-15 08:52:21', '2016-02-15 09:06:27'),
(6, 1, 1, '2016-02-01 00:00:00', '2016-02-16 00:00:00', 1, 0.00, 0, '2016-02-15 09:07:10', '2016-02-15 09:07:13'),
(7, 1, 1, '2016-02-01 00:00:00', '2016-02-17 00:00:00', 1, 0.00, 1, '2016-02-15 10:13:05', '2016-02-15 17:13:05');

-- --------------------------------------------------------

--
-- Table structure for table `lendings`
--

DROP TABLE IF EXISTS `lendings`;
CREATE TABLE IF NOT EXISTS `lendings` (
  `id` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_member` tinyint(1) NOT NULL,
  `member` int(10) unsigned DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` int(10) unsigned NOT NULL,
  `total` double(18,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lendings`
--

INSERT INTO `lendings` (`id`, `is_active`, `is_member`, `member`, `customer`, `payment_method`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '', 1, 0.00, '2016-02-15 08:36:55', '2016-02-15 15:36:56');

-- --------------------------------------------------------

--
-- Table structure for table `member_types`
--

DROP TABLE IF EXISTS `member_types`;
CREATE TABLE IF NOT EXISTS `member_types` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `member_price` double(18,2) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lending_limit` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member_types`
--

INSERT INTO `member_types` (`id`, `code`, `description`, `member_price`, `is_active`, `created_at`, `updated_at`, `lending_limit`) VALUES
(1, 'VIP', 'VIP', 5000.00, 1, '2016-02-15 08:32:50', '2016-02-16 02:18:20', 10);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `id` int(10) unsigned NOT NULL,
  `member_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `member_type` int(10) unsigned DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `member_id`, `member_type`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '797043262', 1, 1, '2016-02-15 08:33:01', '2016-02-15 15:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_01_29_063319_create_access_rights', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_10_31_162633_scaffoldinterfaces', 1),
('2016_01_29_015813_create_table_member_types', 1),
('2016_01_29_063334_create_categories', 1),
('2016_01_29_063347_create_return_statuses', 1),
('2016_01_29_063410_create_payment_method', 1),
('2016_01_29_063427_create_books', 1),
('2016_01_29_063436_create_members', 1),
('2016_01_29_063458_create_lendings', 1),
('2016_02_01_021316_create_table_details', 1),
('2016_02_04_032314_alter_table_details_modify_timestamps', 1),
('2016_02_15_064223_stock_statuses', 1),
('2016_02_15_083900_add_lending_limit_to_member_types', 1),
('2016_02_16_022123_add_isbn_issue_date_qty_price_to_book', 2),
('2016_02_16_022124_add_for_sale_to_books', 2),
('2016_02_16_022713_create_table_sales', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `code`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Cash', 'Cash', 1, '2016-02-15 08:35:46', '2016-02-15 15:35:46'),
(2, 'Credit Car', 'Credit Card', 1, '2016-02-15 08:36:00', '2016-02-15 15:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `return_statuses`
--

DROP TABLE IF EXISTS `return_statuses`;
CREATE TABLE IF NOT EXISTS `return_statuses` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `return_statuses`
--

INSERT INTO `return_statuses` (`id`, `code`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Processoed', 'Completed', 1, '2016-02-15 08:43:08', '2016-02-15 15:43:08'),
(2, 'Returned', 'Returned', 1, '2016-02-15 08:43:20', '2016-02-15 15:43:20'),
(3, 'Damaged', 'Damaged', 1, '2016-02-15 08:43:35', '2016-02-15 15:43:35'),
(4, 'Overdued', 'Overdued', 1, '2016-02-15 09:52:22', '2016-02-15 16:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
CREATE TABLE IF NOT EXISTS `sales` (
  `id` int(10) unsigned NOT NULL,
  `sales_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sales_date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_member` tinyint(1) NOT NULL,
  `member` int(10) unsigned DEFAULT NULL,
  `customer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` int(10) unsigned NOT NULL,
  `grand_total` double(18,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scaffoldinterfaces`
--

DROP TABLE IF EXISTS `scaffoldinterfaces`;
CREATE TABLE IF NOT EXISTS `scaffoldinterfaces` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `views` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tablename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock_statuses`
--

DROP TABLE IF EXISTS `stock_statuses`;
CREATE TABLE IF NOT EXISTS `stock_statuses` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stock_statuses`
--

INSERT INTO `stock_statuses` (`id`, `code`, `description`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Available', 'Available', 1, '2016-02-16', '2016-02-16'),
(2, 'Inverse', 'Reverse', 0, '2016-02-16', '2016-02-16'),
(3, 'Inverse', 'Inverse', 1, '2016-02-16', '2016-02-16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `access_right` int(10) unsigned DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `is_active`, `access_right`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'wandi', 'nandermind2@gmail.com', '$2y$10$6Jq2lFPAFEdT.ZqLjT.uauxGIMV1mzw5ePVAKK5PL.JIk3vSE9l6K', 1, 4, NULL, '0000-00-00 00:00:00', '2016-02-15 20:26:50'),
(2, 'wandi', 'nandermind3@gmail.com', '$2y$10$ssk9un.nk0BVSnuD.Qa1a.W1fzKNpYaKeclI02gOpmIVDPlDPL12S', 1, 2, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'wandi', 'nandermind4@gmail.com', '$2y$10$aFcWeuiMcZzaarYWNVoWCuysgmtRucHskFQNHgF6S6JUKortg.q66', 1, 3, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_rights`
--
ALTER TABLE `access_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_categories_foreign` (`categories`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `details_lending_id_foreign` (`lending_id`),
  ADD KEY `details_book_foreign` (`book`),
  ADD KEY `details_status_foreign` (`status`);

--
-- Indexes for table `lendings`
--
ALTER TABLE `lendings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lendings_member_foreign` (`member`),
  ADD KEY `lendings_payment_method_foreign` (`payment_method`);

--
-- Indexes for table `member_types`
--
ALTER TABLE `member_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_member_type_foreign` (`member_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `return_statuses`
--
ALTER TABLE `return_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_member_foreign` (`member`),
  ADD KEY `sales_payment_method_foreign` (`payment_method`);

--
-- Indexes for table `scaffoldinterfaces`
--
ALTER TABLE `scaffoldinterfaces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_statuses`
--
ALTER TABLE `stock_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_access_right_foreign` (`access_right`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_rights`
--
ALTER TABLE `access_rights`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `lendings`
--
ALTER TABLE `lendings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member_types`
--
ALTER TABLE `member_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `return_statuses`
--
ALTER TABLE `return_statuses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scaffoldinterfaces`
--
ALTER TABLE `scaffoldinterfaces`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_statuses`
--
ALTER TABLE `stock_statuses`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_categories_foreign` FOREIGN KEY (`categories`) REFERENCES `categories` (`id`);

--
-- Constraints for table `details`
--
ALTER TABLE `details`
  ADD CONSTRAINT `details_book_foreign` FOREIGN KEY (`book`) REFERENCES `books` (`id`),
  ADD CONSTRAINT `details_lending_id_foreign` FOREIGN KEY (`lending_id`) REFERENCES `lendings` (`id`),
  ADD CONSTRAINT `details_status_foreign` FOREIGN KEY (`status`) REFERENCES `return_statuses` (`id`);

--
-- Constraints for table `lendings`
--
ALTER TABLE `lendings`
  ADD CONSTRAINT `lendings_member_foreign` FOREIGN KEY (`member`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `lendings_payment_method_foreign` FOREIGN KEY (`payment_method`) REFERENCES `payment_methods` (`id`);

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_member_type_foreign` FOREIGN KEY (`member_type`) REFERENCES `member_types` (`id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_member_foreign` FOREIGN KEY (`member`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `sales_payment_method_foreign` FOREIGN KEY (`payment_method`) REFERENCES `payment_methods` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_access_right_foreign` FOREIGN KEY (`access_right`) REFERENCES `access_rights` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
