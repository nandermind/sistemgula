<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Models\Foods;
	use Carbon\Carbon;
	use DB;
	use Session;
	use View;
	use Input;
	use Hash;
	use Illuminate\Pagination\LengthAwarePaginator;
	use Illuminate\Pagination\Paginator;
	class FoodsController extends controller
	{
		public function showUser()
			{
				$list = Foods::paginate(15);
				return view('foods.index')->with('list',$list);
			}
		public function showNewUser()
			{
				$obj = new Foods();
				return view('foods.create_form')->with('obj',$obj);
			}
		public function createNewUser(Request $request)
			{
					$obj = new Foods($request->all());
					$obj->foodName = $request->foodName;
					$obj->glucoseCount = $request->glucoseCount;
					$obj->save();
					Session::flash("messages", "Save Success !");
					return redirect('/foods');
	        }
		public function deleteUser($id)
			{
				$User  		   	  = Foods::where('idfoods','=',$id);
				$User->delete();
				Session::flash('message', 'Successfully Updated User Status');
				return redirect('/foods');
			}
	}

?>