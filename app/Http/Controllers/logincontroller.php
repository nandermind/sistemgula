<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use session;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Flash;
use DB;
class LoginController extends controller
{
	public function showLogin()
	{
		return view('login');
	}
	public function doLogin(Request $request)
	{
		$data = User::where("iduser","=",$request->input('email'))
		->where("password","=",$request->input('password'))->first();
		//if account is exists
		if ($data) 
		{
			return view('superuser.index');
		}
		else
		{
			Flash::error('Invalid Username/ Password');
			return view('login');
		}
	}
       	//logout
	public function logout()
	{
		Auth::logout();
		return redirect('login');	
	}
}
?>