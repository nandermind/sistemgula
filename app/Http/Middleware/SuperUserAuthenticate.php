<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;

class SuperUserAuthenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //if superuser
        if (Auth::check() && Auth::user()->access_right == 1)
        {
            return $next($request);
        }
        //if admin
        else if (Auth::check() && Auth::user()->access_right == 2)
        {
            return redirect('admin_panel');
        }
        //if librarian
        else if (Auth::check() && Auth::user()->access_right == 3)
        {
            return redirect('librarian_panel');
        }
        return redirect()->guest('/login');
    }
}
