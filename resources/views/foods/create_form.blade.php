	@extends('layouts.template_superuser')
	@section('header')
		<p>FOOD LIST - CREATE</p>
	@endsection
	@section('content')
	{!! Form::open(array('url'=>'/foods/insert','method' => '{{ $method }}'))!!}
		<table class="table table-responsive">
			<tr>
					<td>{!!Form::label('foodName','Food Name') !!}</td>
					<td>
						<input type="text" required name="foodName"></input>
					</td>
			</tr>
			<tr>
					<td>{!!Form::label('glucoseCount','Glucose Count') !!}</td>
					<td>
						<input type="text" required name="glucoseCount"></input>
					</td>
			</tr>
		</table>
	@endsection
	@section('content2')
		<table class="table table-responsive">
			<tr>
				<td colspan="2">
					<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"> Submit</span></button>
				</td>
			</tr>
		</table>
		{!! Form::close() !!}
	@endsection
