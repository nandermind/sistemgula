<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/login', 'LoginController@showLogin');
Route::get('/logout', 'LoginController@logout');
Route::POST('/login', 'LoginController@doLogin');
route::get('index','LoginController@SuccessLogin');
route::get('admin_panel','LoginController@SuccessLoginAdmin');
route::get('librarian_panel','LoginController@SuccessLoginLibrarian');

		//user
Route::get('/user','UserController@showUser');
Route::get('/user/new','UserController@showNewUser');
Route::POST('/user/insert','UserController@createNewUser');
Route::get('/user/updating/{id}','UserController@showEditUser');
Route::POST('/user/update','UserController@UpdateUser');
Route::get('/user/drop/{id}','UserController@deleteUser');
Route::resource('/user/bulkupdate', 'UserController@updateAccessRight');

//user
Route::get('/foods','FoodsController@showUser');
Route::get('/foods/new','FoodsController@showNewUser');
Route::POST('/foods/insert','FoodsController@createNewUser');
Route::get('/foods/updating/{id}','FoodsController@showEditUser');
Route::POST('/foods/update','FoodsController@UpdateUser');
Route::get('/foods/drop/{id}','FoodsController@deleteUser');
Route::resource('/foods/bulkupdate', 'FoodsController@updateAccessRight');

//register item
Route::get('/log/new','LogsController@showNewUser');
Route::POST('/log/insert','LogsController@createNewUser');
		
Route::get('error/503',function(){
	return view('errors.503');
});

Route::get('/', function () {
	return view('login');
});